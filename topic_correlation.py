import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

from gensim.models import LdaModel
from gensim.corpora import Dictionary

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from nltk.stem.porter import PorterStemmer

tokenizer = RegexpTokenizer(r'\w+')
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))
stemmer = PorterStemmer()

def tokenize(string):
    normalized_tokens = list()
    tokens = tokenizer.tokenize(string)
    for token in tokens:
        if token.lower() not in stop_words and len(token) > 1:
            normalized = token.lower()
            normalized = stemmer.stem(normalized)
            normalized_tokens.append(normalized)
    return normalized_tokens

try:
    dictionary = Dictionary.load('topic_correlation/dictionary.dict')
    ldamodel = LdaModel.load('topic_correlation/ldamodel.model')
except:
    print("Reading datasets...")
    test_balanced = pd.read_csv('data/test-balanced.csv.bz2', compression='bz2')
    train_balanced = pd.read_csv('data/train-balanced.csv.bz2', compression='bz2')

    #columns = ["label", "comment", "author", "subreddit", "score", "ups", "downs", "date", "created_utc", "parent_comment"]
    #train_balanced = pd.read_csv("http://nlp.cs.princeton.edu/SARC/1.0/main/train-balanced.csv.bz2", compression="bz2", sep="\t", header=None, names=columns)
    #test_balanced = pd.read_csv("http://nlp.cs.princeton.edu/SARC/1.0/main/test-balanced.csv.bz2", compression="bz2", sep="\t", header=None, names=columns)

    data_types = {
        "label": "int",
        "comment": "unicode",
        "author": "unicode",
        "subreddit": "unicode",
        "score": "float",
        "ups": "float",
        "downs": "float",
        "date": "object",
        "created_utc": "object",
        "parent_comment": "unicode"
    }

    df = pd.concat((train_balanced, test_balanced)).astype(data_types)
    #n_samples = 100000
    #df = pd.concat((
    #    train_balanced[train_balanced['label'] == 1].sample(n=int(n_samples/2)),
    #    train_balanced[train_balanced['label'] == 0].sample(n=int(n_samples/2))
    #)).sample(frac=1).astype(data_types)

    print("Tokenizing text...")
    tokenized_text = df['comment'].apply(tokenize)
    print("Creating dictionary...")
    dictionary = Dictionary(tokenized_text)
    print("Creating bag of words corpus...")
    corpus = [dictionary.doc2bow(text) for text in tokenized_text]

    print("Creating LDAModel...")
    ldamodel = LdaModel(corpus, num_topics=100, id2word=dictionary)

    ldamodel.save('topic_correlation/ldamodel.model')
    dictionary.save('topic_correlation/dictionary.dict')

print("Getting topics")
topics = ldamodel.get_topics()

print("Calculating correlation...")
topics_corr = pd.DataFrame(data=topics.T).corr()
np.savetxt('topic_correlation/corr.csv', topics_corr)

for i, row in enumerate(np.array(topics_corr)):
    for j, n in enumerate(row[i+1:]):
        if (abs(n) > 0.1):
            print("(%d, %d)" % (i, j), n)
            print(ldamodel.print_topic(i))
            print(ldamodel.print_topic(j))
            print("\n")

plt.figure()
plt.matshow(topics_corr, vmin=-1, vmax=1)
plt.colorbar()
plt.title("Topic Correlation", pad=12)
plt.savefig('topic_correlation/corr.png')
#plt.show()

print("Done!")
# Sarcasm Detection Level 4 Project

Dominic Small, 2195530s

Level 4 Individual Project to evaluate methods of sarcasm detection in online discourse.

## How to use

The source code consists of two files: one jupyter notebook (SarcasmDetectionInOnlineDiscourse.ipynb) and one python script (SarcasmDetectionInOnlineDiscourse.py).

Both perform the experiment described in the dissertation in the same way
but they have both been provided so that whichever
is the more convinient way of running the experiment can be used.

The experiment will create four sub-directories:

- Data, to store the datasets (these will be downloaded if not already present)
- Models, to save models (classifiers, lda models, etc)
- Tables, to store .csv files of experiment data for creating tables
- Graphs, for saving .png files of various graphs created

To run the experiment, either open the jupyter notebook and select Cells -> Run All, or run the python script with the command `python SarcasmDetectionInOnlineDiscourse.py`

Be aware that the experiment will take several hours to complete and requires a significant amount of memory. Steps have been taken to reduce the amount of memory needed, but it will still require a minimum of ~8GB of RAM to run completely.

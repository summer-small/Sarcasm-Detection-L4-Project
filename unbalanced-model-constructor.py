# Import Packages

import pandas as pd
import numpy as np

import nltk
import json

from gensim.models.ldamodel import  LdaModel
from gensim.corpora import Dictionary, HashDictionary

from datetime import datetime, timedelta

from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from nltk.stem.porter import PorterStemmer

# Setup tokenizer

tokenizer = RegexpTokenizer(r'\w+')
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))
stemmer = PorterStemmer()

def tokenize(string):
    normalized_tokens = list()
    tokens = tokenizer.tokenize(string)
    for token in tokens:
        if token.lower() not in stop_words and len(token) > 1 and len(token) < 24:
            normalized = token.lower()
            normalized = stemmer.stem(normalized)
            normalized_tokens.append(normalized)
    return normalized_tokens

# Initialize variables

columns = ["label", "comment", "author", "subreddit", "score", "ups", "downs", "date", "created_utc", "parent_comment"]

today = datetime(2017, 5, 1) # SARC goes up to april 2017
last_month = today - timedelta(weeks=4)
last_six_months = last_month - timedelta(weeks=24)
last_year = last_six_months - timedelta(days=365)

zero_times = {
    'last_month': 0,
    'last_six_months': 0,
    'last_year': 0,
    'earlier': 0
}

chunksize = 1000

data_types = {
    "label": "int",
    "comment": "unicode",
    "author": "unicode",
    "subreddit": "unicode",
    "score": "float",
    "ups": "float",
    "downs": "float",
    "date": "object",
    "created_utc": "object",
    "parent_comment": "unicode"
}

start = datetime.now()
print("Started at:", start)

# Initialize models

author_history = {}
subreddit_history = {}
dictionary = HashDictionary()
ldamodel = LdaModel(corpus=None, num_topics=100, id2word=dictionary)

# Build models

i = 0
for df in pd.read_csv("data/train-unbalanced.csv.bz2", compression='bz2', sep="\t", header=None, names=columns, chunksize=chunksize):
    df = df.astype(data_types)
    df['datetime'] = pd.to_datetime(df['created_utc'], unit='s')
    if (i % 1000 == 0):
        print("Processing chunk {} - {}s".format(i, (datetime.now() - start).seconds))
        
    tokenized_text = list()
        
    for _, post in df.iterrows():
        # Author Sarcasm History
        if post['author'] not in author_history:
            author_history[post['author']] = {'0': zero_times.copy(), '1': zero_times.copy()}
        if (post['datetime'] >= last_month):
            time = 'last_month'
        elif (post['datetime'] >= last_six_months):
            time = 'last_six_months'
        elif (post['datetime'] >= last_year):
            time = 'last_year'
        else:
            time = 'earlier'

        author_history[post['author']][str(post['label'])][time] += 1

        # Subreddit Sarcasm History
        if post['subreddit'] not in subreddit_history:
            subreddit_history[post['subreddit']] = {'0': 0, '1': 1}

        subreddit_history[post['subreddit']][str(post['label'])] += 1
        
        # Tokenize comments
        tokenized_text.append(tokenize(post['comment']))

    # Topic Modelling
    dictionary.add_documents(tokenized_text)
    corpus = [dictionary.doc2bow(text) for text in tokenized_text]
    ldamodel.update(corpus)
    
    i += 1

# Save models

with open('unbalanced_models/author_history.json', 'w') as f:
    json.dump(author_history, f)
    
with open('unbalanced_models/subreddit_history.json', 'w') as f:
    json.dump(subreddit_history, f)
    
ldamodel.save('unbalanced_models/ldamodel.model')
dictionary.save('unbalanced_models/dictionary.dict')